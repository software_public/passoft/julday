======
julday
======


* Description: displays julian date.

* Usage: julday mm dd
         julday mm dd yyyy

* Free software: GNU General Public License v3 (GPLv3)
