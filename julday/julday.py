#!/usr/bin/env python3

"""
julday.py given a calendar date will convert the date into its equivalent
julian date and prints both.

# Shannen Lowe
# 05-21-2018

Maeva Pourpoint
July 2020
Updates to work under Python 3.
Unit tests to ensure basic functionality.
Code cleanup to conform to the PEP8 style guide.
Directory cleanup (remove unused files introduced by Cookiecutter).
Packaged with conda.
"""

import sys
import datetime


def usage():
    """Prints the usage."""
    print('Usage : {} mm dd [year]'.format(sys.argv[0]))


def cal_to_jul(argv):
    """
    Takes the calendar date given and prints both the calendar date and it's
    julian date equivalent.
    """
    if len(argv) > 0:
        month = argv[0].zfill(2)
        day = argv[1].zfill(2)
        if len(argv) == 3:
            year = argv[2].zfill(4)
        else:  # Use current year if no year given.
            year = datetime.datetime.today().strftime("%Y")
        calendar_date = "{} {} {}".format(month, day, year)
    else:
        calendar_date = datetime.datetime.today().strftime("%m %d %Y")

    if is_date_valid(calendar_date):
        print('\n Calendar Date {}'.format(calendar_date))
        # Parse string to a datetime object.
        d = datetime.datetime.strptime(calendar_date, "%m %d %Y")
        # Format d to a julian date that can be printed.
        julian = d.strftime("%j %Y")
        print(' Julian Date {}\n'.format(julian))
    else:
        print('ERROR ', end='')
        if int(month) < 1 or int(month) > 12:
            print('Month = {} is not a valid month'.format(month))
        else:
            print('Date {} {} {} is not a valid date'.format(month, day, year))
        sys.exit(1)


def is_date_valid(calendar_date):
    """Check if the date of the year is valid."""
    try:
        # strptime will raise an error if day, month, or year is out of range
        datetime.datetime.strptime(calendar_date, "%m %d %Y")
        return True
    except ValueError:
        return False


def main():
    """
    julday takes either no arguments, the month and day, or a month, day, and
    year as arguments
    """
    if len(sys.argv) not in [1, 3, 4]:
        usage()
        sys.exit(1)
    cal_to_jul(sys.argv[1:])


if __name__ == "__main__":
    main()
