#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `julday` package."""

import io
import unittest

from unittest.mock import patch
from julday.julday import is_date_valid, cal_to_jul

VALID_DATE = ['04', '02', '2020']
UNVALID_MONTH = ['13', '02', '2020']
UNVALID_YEAR = ['04', '02', '-2050']


class TestJulday(unittest.TestCase):
    """Tests for `julday` package."""

    def test_is_date_valid(self):
        """Test basic functionality of is_date_valid function"""
        calendar_date_valid = "{} {} {}".format(VALID_DATE[0],
                                                VALID_DATE[1],
                                                VALID_DATE[2])
        calendar_date_unvalid_1 = "{} {} {}".format(UNVALID_MONTH[0],
                                                    UNVALID_MONTH[1],
                                                    UNVALID_MONTH[2])
        calendar_date_unvalid_2 = "{} {} {}".format(UNVALID_YEAR[0],
                                                    UNVALID_YEAR[1],
                                                    UNVALID_YEAR[2])
        self.assertTrue(is_date_valid(calendar_date_valid),
                        "{} is not a valid date".format(calendar_date_valid))
        self.assertFalse(is_date_valid(calendar_date_unvalid_1),
                         "{} is a valid date".format(calendar_date_unvalid_1))
        self.assertFalse(is_date_valid(calendar_date_unvalid_2),
                         "{} is a valid date".format(calendar_date_unvalid_2))

    @patch('julday.julday.sys.exit', autospec=True)
    @patch('sys.stdout', new_callable=io.StringIO)
    def test_cal_to_jul(self, mock_stdout, mock_exit):
        """Test basic functionality of cal_to_jul function"""
        cal_to_jul(VALID_DATE)
        output = '\n Calendar Date 04 02 2020\n Julian Date 093 2020\n\n'
        self.assertEqual(mock_stdout.getvalue(), output,
                         "Failed to properly convert date!")
        cal_to_jul(UNVALID_MONTH)
        self.assertTrue(mock_exit.called, "sys.exit(1) never called - "
                        "cal_to_jul() not fully exercised!")
        cal_to_jul(UNVALID_YEAR)
        self.assertTrue(mock_exit.called, "sys.exit(1) never called - "
                        "cal_to_jul() not fully exercised!")
